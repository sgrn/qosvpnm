#! /bin/bash
# \file operation.sh
# \brief Contains all server oriented functionalities.
# \author sangsoic <sangsoic@protonmail.com>
# \version 0.1
# \date 2020-05-16 Sat 03:55 PM
#
# \copyright Copyright 2020 Sangsoic author
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
# 
#             http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


# \fn deselect_server ()
# \brief Deselects currently selected server.
# \author sangsoic <sangsoic@protonmail.com>
# \version 0.1
# \date 2020-05-16 Sat 12:14 PM
# \return 0 if deselection went fine else 1
deselect_server ()
{
	local name exitStatus
	if [[ -f /rw/config/vpn/.selected ]]; then
		name=`head -n 1 /rw/config/vpn/.selected`
		echo deselect "$name" VPN server.
		mv /rw/config/vpn/servers/."$name" /rw/config/vpn/servers/"$name"
		rm /rw/config/vpn/.selected /rw/config/vpn/openvpn-client.ovpn
		exitStatus=0
	else
		echo no VPN server currently selected. 1>&2
		exitStatus=1
	fi
	return $exitStatus
}

# \fn display_selected_server ()
# \brief Displays currently selected server name.
# \author sangsoic <sangsoic@protonmail.com>
# \version 0.1
# \date 2020-05-16 Sat 12:14 PM
# \return 0 if a server is currently selected else 1
display_selected_server ()
{
	local exitStatus
	echo == CURRENT SELECTED SERVER ==
	if [[ -f /rw/config/vpn/.selected ]]; then
		head -n 1 /rw/config/vpn/.selected
		exitStatus=0
	else
		echo error : no VPN server currently selected. 1>&2
		exitStatus=1
	fi
	echo == END ==
	return $exitStatus
}

# \fn select_server ()
# \brief Selects zero, one, or more server depending on a given pattern.
# \author sangsoic <sangsoic@protonmail.com>
# \version 0.1
# \date 2020-05-16 Sat 12:14 PM
# \param 1 A given pattern.
# \return 0 if a server is currently selected else 1
select_server ()
{
	local servers server name exitStatus
	declare -a arrServers
	if servers=`ls /rw/config/vpn/servers/$1 2>/dev/null`; then
		deselect_server
		arrServers=($servers)
		server="${arrServers[$(( RANDOM % ${#arrServers[*]} ))]}"
		name="${server##*/}"
		cp "$server" /rw/config/vpn/openvpn-client.ovpn
		echo -e -n "$name\n$1" > /rw/config/vpn/.selected
		echo "${name%.*}" >> /rw/config/vpn/.history
		mv "$server" /rw/config/vpn/servers/."$name"
		echo "$name" VPN server has been selected.
		echo restart for changes to take effect !
		exitStatus=0
	else
		echo error: "$1" do not pattern with any VPN file. 1>&2
		exitStatus=1
	fi
	return $exitStatus
}

# \fn refresh_selected_server ()
# \brief Refreshes currently selected server using previously given pattern.
# \author sangsoic <sangsoic@protonmail.com>
# \version 0.1
# \date 2020-05-16 Sat 12:14 PM
# \return 0 if a server is currently selected else 1
refresh_selected_server ()
{
	local pattern exitStatus
	if [[ -f /rw/config/vpn/.selected ]]; then
		pattern=`tail -n 1 /rw/config/vpn/.selected`
		select_server "$pattern"
		exitStatus=0
	else
		echo no VPN server currently selected. 1>&2
		exitStatus=1
	fi
	return $exitStatus
}

# \fn list_server ()
# \brief Lists server names that match with a given pattern.
# \author sangsoic <sangsoic@protonmail.com>
# \version 0.1
# \date 2020-05-16 Sat 12:14 PM
# \param 1 A given pattern.
# \return 0 if pattern matches with at least one server else 1
list_server ()
{
	local servers exitStatus
	echo === SERVER_LIST ===
	if servers=`ls /rw/config/vpn/servers/$1 2>/dev/null`; then
		for server in $servers; do
			server=${server##*/}
			server=${server%.*}
			echo ${server}
		done
		exitStatus=0
	else
		echo error: "$1" do not pattern with any VPN file. 1>&2
		exitStatus=1
	fi
	echo === END ===
	return $exitStatus
}

# \fn list_server_from_profile ()
# \brief Lists server name(s) via pattern saved in the given profile.
# \author sangsoic <sangsoic@protonmail.com>
# \version 0.1
# \date 2020-05-16 Sat 12:14 PM
# \param 1 A given profile name.
# \return 0 if the given profile exists and leads to at least one server else 1
list_server_from_profile ()
{
	local pattern exitStatus
	if [[ -f /rw/config/vpn/profiles/"$1" ]]; then 
		pattern=`cat /rw/config/vpn/profiles/"$1"`
		list_server "$pattern"
		exitStatus=$?
	else
		echo error: "$1" do not pattern any VPN profile file. 1>&2
		exitStatus=1
	fi
	return $exitStatus
}

# \fn list_server_from_profile ()
# \brief Selects server(s) via pattern saved in the given profile.
# \author sangsoic <sangsoic@protonmail.com>
# \version 0.1
# \date 2020-05-16 Sat 12:14 PM
# \param 1 A given profile name.
# \return 0 if the given profile exists and leads to at least one server else 1
select_server_from_profile ()
{
	local pattern exitStatus
	if [[ -f /rw/config/vpn/profiles/"$1" ]]; then 
		pattern=`cat /rw/config/vpn/profiles/"$1"`
		select_server "$pattern"
		exitStatus=$?
	else
		echo error: "$1" do not pattern any VPN profile file. 1>&2
		exitStatus=1
	fi
	return $exitStatus
}

# \fn list_profile ()
# \brief Lists all profiles.
# \author sangsoic <sangsoic@protonmail.com>
# \version 0.1
# \date 2020-05-16 Sat 12:14 PM
list_profile ()
{
	echo == PROFILES ==
	ls /rw/config/vpn/profiles/
	echo == END ==
}

# \fn save_profile ()
# \brief Saves a given pattern as a profile.
# \author sangsoic <sangsoic@protonmail.com>
# \version 0.1
# \date 2020-05-16 Sat 12:14 PM
# \param 1 A given profile name.
# \param 2 A given pattern.
# \return 0 if profile was created else 1
save_profile ()
{
	local saveOrNot exitStatus
	exitStatus=1
	if [[ -n "$1" ]]; then
		if list_server "$2"; then
			read -p "Are you sure you want to save this profile ? Y/n: " saveOrNot
			[[ "$saveOrNot" =~ ^[yY]$ ]] && {
				echo -n "$2" > /rw/config/vpn/profiles/"$1"
				echo "$1" VPN server profile saved.
			}
			exitStatus=0
		fi
	else
		echo error: invalid name "'$1'". 1>&2
	fi
	return $exitStatus
}

# \fn remove_profile ()
# \brief Removes a given profile.
# \author sangsoic <sangsoic@protonmail.com>
# \version 0.1
# \date 2020-05-16 Sat 12:14 PM
# \param 1 A given profile name.
# \return 0 if profile was removed else 1
remove_profile ()
{
	local rmOrNot exitStatus
	if [[ -f /rw/config/vpn/profiles/"$1" ]]; then
		read -p "Are you sure you want to remove this profile ? Y/n: " rmOrNot
		[[ "$rmOrNot" =~ ^[yY]$ ]] && {
			rm /rw/config/vpn/profiles/"$1"
			echo "$1" VPN server profile removed.
		}
		exitStatus=0
	else
		echo error: invalid name "'$1'". 1>&2
		exitStatus=1
	fi
	return $exitStatus
}

# \fn list_connection_history ()
# \brief Displays history.
# \author sangsoic <sangsoic@protonmail.com>
# \version 0.1
# \date 2020-05-16 Sat 12:14 PM
list_connection_history ()
{
	echo == LIST HISTORY ==
	if [ -f /rw/config/vpn/.history -a -s /rw/config/vpn/.history ]; then
		cat /rw/config/vpn/.history
	else
		echo error no history available. 1>&2
	fi
	echo == END ==
}

# \fn remove_connection_history ()
# \brief Removes history.
# \author sangsoic <sangsoic@protonmail.com>
# \version 0.1
# \date 2020-05-16 Sat 12:14 PM
remove_connection_history ()
{
	rm -v /rw/config/vpn/.history
}
