# QOSVPNM

*if you have any request regarding the project please contact the author at <sangsoic@protonmail.com>*

QOSVPNM is a simple BASH utility that provides rich functionalities enhancing and facilitating VPN connections under Qubes OS.

![interactive menu](Img/menu.png)

## Pre-installation.

1. Create an new app VM, install `openvpn` and disable the auto-start service just as mentioned as the first step [here](https://github.com/Qubes-Community/Contents/blob/master/docs/configuration/vpn.md#set-up-a-proxyvm-as-a-vpn-gateway-using-iptables-and-cli-scripts)

2. Once the first step is completed, run the newly created *VPN APP VM*.
## Setup.

*All the instructions of this section must be run inside the VPN APP VM.*
1. Clone this repository.

	`git clone https://gitlab.com/sangsoic/qosvpnm.git`

2. Change your current location.

	`cd QOSVPNM`

3. Make setup script executable and run it.

	`chmod u+x setup_app.sh`

	`sudo ./setup_app.sh -i`

	If you whish to uninstall QOSVPNM, run this :

	`sudo ./setup_app -u`

	Once the setup script was run successfully, you can start using QOSVPNM.

## Functionalities.

*This section describes how to properly use QOSVPNM.*
*QOSVPNM must always be run with root privileges.*

### Run using the user's menu.

* Execute QOSVPNM in **user menu** mode.

	`sudo qosvpnm -m` or `sudo qosvpnm`

* Display help for CLI options by using the `-h` option.

#### Setup your VPN.

Once you are inside the **user menu** hit `2` to setup your VPN inside, and follow the instructions.  
The last setup instruction opens a bash shell with root privileges at */rw/config/vpn/servers* which is where you must copy your .ovpn configuration files.  
After you copied all the configuration files there, type the `exit` command to exit the shell.  
To add more server files in the future, you must repeat this process or just copy the files directly in the */rw/config/vpn/servers* directory.

#### Server management.

*Selecting a server A, means that next time your VPN APP VM will reboot, the client will connect to the server A.*

To select one or more server(s) hit `3` and follow the instructions.

*A pattern is a sequence of characters (possibly shell compatible wildcards) that matches with zero, one or more server name(s) inside the /rw/config/vpn/servers directory.*

If the pattern matches with more than one server name, it will be selected randomly among the list of names that matches with the given pattern.

Once a server has been selected, the VPN client will connect to it at after booting.

Options **3 to 6** concern server management. Hit enter to display them as well as their description.

#### Profile management.

*A profile is a saved pattern that could be use for server selection or listing.*

Options **7 to 11** concern profile management. Hit enter to display them as well as their description.

*Selecting a profile A, means selecting a server using the saved pattern of the profile A.*

#### Change your VPN credentials.

Option 12 allows you to change you VPN credentials.

#### List and Remove connection history.

*The history corresponds to the list of servers previously selected.*

You can either list (option `13`) or remove (option `14`) your history.

### Refresh current server selection.

When you execute QOSVPNM you can either use the **user menu** mode or the **automatic refresh** mode.

The **automatic refresh** mode is useful only once you selected a server and entered a pattern that matches with more than one server name. It allows you to randomly change the current selected server, using the same pattern you entered during the last server selection.

`sudo qosvpnm -r`

Every time you restart the *VPN APP VM* it refreshes the last server selection.
